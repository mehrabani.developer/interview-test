<?php

use Illuminate\Support\Facades\Route;
use Service\Order\Http\Controllers\OrderController;

Route::group(['prefix' => 'api'], function () {

    Route::get('orders', [OrderController::class, 'index']);
    Route::post('orders', [OrderController::class, 'store']);
    Route::get('orders/{order}', [OrderController::class, 'show']);

    // change status
    Route::patch('orders/{order}', [OrderController::class, 'update']);
});
