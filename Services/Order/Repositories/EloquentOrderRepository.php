<?php

namespace Service\Order\Repositories;

use Service\Order\Contracts\OrderRepository;
use Service\Order\Models\Order;

class EloquentOrderRepository implements OrderRepository
{
    public function paginate($perPage)
    {
        return Order::paginate($perPage);
    }

    public function create($data)
    {
        return Order::create($data);
    }

    public function update($id, $data)
    {
        $review = Order::find($id);
        $review->update($data);
        return $review;
    }

    public function find($id)
    {
        return Order::find($id);
    }
}
