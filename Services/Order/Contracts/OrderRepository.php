<?php

namespace Service\Order\Contracts;

interface OrderRepository
{
    public function paginate($perPage);
    public function create($data);
    public function update($id, $data);
    public function find($id);
}
