<?php

namespace Service\Order\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Service\Order\Contracts\OrderRepository;
use Service\Order\Http\Requests\OrderStoreRequest;
use Service\Order\Http\Requests\OrderUpdateRequest;
use Service\Order\Http\Responses\Responses;

class OrderController
{
    private $orderRepository;

    public function __construct(
        OrderRepository $orderRepository
    )
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Service\Order\Http\Responses\Responses
     */
    public function index()
    {
        return Responses::index(
            $this->orderRepository->paginate(request('size', 20))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrderStoreRequest $request
     * @return \Service\Order\Http\Responses\Responses
     */
    public function store(OrderStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $order = $this->orderRepository->create($request->all());
            DB::commit();
            $response = Responses::successStore($order);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedStore();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Service\Order\Http\Responses\Responses
     */
    public function show($id)
    {
        try {
            $response = Responses::successShow(
                $this->orderRepository->find($id)
            );
        } catch (\Exception $e) {
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedShow($id);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrderUpdateRequest $request
     * @param int $id
     * @return \Service\Order\Http\Responses\Responses
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $order = $this->orderRepository->update($id, $request->all());
            DB::commit();
            $response = Responses::successUpdate($order);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedUpdate($id);
        }

        return $response;
    }
}
