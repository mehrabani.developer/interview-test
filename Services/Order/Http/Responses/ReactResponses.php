<?php

namespace Service\Order\Http\Responses;

use Illuminate\Http\Response;
use Service\Order\Contracts\ResponseInterface;
use Service\Order\Http\Resources\OrderResource;

class ReactResponses implements ResponseInterface
{
    public function index($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'False',
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function successStore($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'False',
            'status' => 'success',
            'data' => new OrderResource($data)
        ], Response::HTTP_CREATED);
    }

    public function failedStore($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }

    public function successShow($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'False',
            'status' => 'success',
            'data' => new OrderResource($data)
        ]);
    }

    public function failedShow($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }

    public function successUpdate($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'False',
            'status' => 'success',
            'data' => new OrderResource($data)
        ], Response::HTTP_CREATED);
    }

    public function failedUpdate($data = null)
    {
        return response()->json([
            'platform' => 'React',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }
}
