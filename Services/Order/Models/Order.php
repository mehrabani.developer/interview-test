<?php

namespace Service\Order\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
      'amount',
      'status',
      'user_id',
      'product_id',
    ];
}
