<?php

namespace Service\Order;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Service\Order\Contracts\OrderRepository;
use Service\Order\Repositories\EloquentOrderRepository;

class OrderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(OrderRepository::class, EloquentOrderRepository::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom([
            base_path('Services/Order/Database/migrations')
        ]);

        Route::group([], base_path('Services/Order/routes/api.php'));
    }
}
