<?php

namespace Service\User\Contracts;

interface UserRepository
{
    public function paginate($perPage);
    public function create($data);
    public function update($id, $data);
    public function find($id);
}
