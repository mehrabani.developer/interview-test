<?php

namespace Service\User\Contracts;

interface ResponseInterface
{
    public function index($data = null);
    public function successStore($data = null);
    public function failedStore($data = null);
    public function successShow($data = null);
    public function failedShow($data = null);
    public function successUpdate($data = null);
    public function failedUpdate($data = null);
}
