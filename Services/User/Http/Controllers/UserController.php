<?php

namespace Service\User\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Service\User\Contracts\UserRepository;
use Service\User\Http\Requests\UserStoreRequest;
use Service\User\Http\Requests\UserUpdateRequest;
use Service\User\Http\Responses\Responses;

class UserController
{
    private $userRepository;

    public function __construct(
        UserRepository $userRepository
    )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Service\User\Http\Responses\Responses
     */
    public function index()
    {
        return Responses::index(
            $this->userRepository->paginate(request('size', 20))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserStoreRequest $request
     * @return \Service\User\Http\Responses\Responses
     */
    public function store(UserStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->create($request->all());
            DB::commit();
            $response = Responses::successStore($user);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedStore();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Service\User\Http\Responses\Responses
     */
    public function show($id)
    {
        try {
            $response = Responses::successShow(
                $this->userRepository->find($id)
            );
        } catch (\Exception $e) {
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedShow($id);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param int $id
     * @return \Service\User\Http\Responses\Responses
     */
    public function update(UserUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->update($id, $request->all());
            DB::commit();
            $response = Responses::successUpdate($user);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedUpdate($id);
        }

        return $response;
    }
}
