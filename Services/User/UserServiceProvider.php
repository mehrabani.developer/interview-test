<?php

namespace Service\User;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Service\User\Contracts\UserRepository;
use Service\User\Repositories\EloquentUserRepository;

class UserServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(UserRepository::class, EloquentUserRepository::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom([
            base_path('Services/User/Database/migrations')
        ]);

        Route::group([], base_path('Services/User/routes/api.php'));
    }
}
