<?php

namespace Service\User\Repositories;

use Service\User\Contracts\UserRepository;
use Service\User\Models\User;

class EloquentUserRepository implements UserRepository
{
    public function paginate($perPage)
    {
        return User::paginate($perPage);
    }

    public function create($data)
    {
        return User::create($data);
    }

    public function update($id, $data)
    {
        $review = User::find($id);
        $review->update($data);
        return $review;
    }

    public function find($id)
    {
        return User::find($id);
    }
}
