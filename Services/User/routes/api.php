<?php

use Illuminate\Support\Facades\Route;
use Service\User\Http\Controllers\UserController;

Route::group(['prefix' => 'api'], function () {
    Route::get('users', [UserController::class, 'index']);
    Route::post('users', [UserController::class, 'store']);
    Route::get('users/{user}', [UserController::class, 'show']);
    Route::patch('users/{user}', [UserController::class, 'update']);
});
