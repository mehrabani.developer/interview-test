<?php

namespace Service\Review\Contracts;

interface ReviewRepository
{
    public function paginate($perPage);
    public function create($data);
    public function update($id, $data);
    public function find($id);
    public function getAvgAndCountWhereProductId($productId);
}
