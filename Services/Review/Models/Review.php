<?php

namespace Service\Review\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
      'user_id',
      'product_id',
      'comment',
      'vote',
      'status'
    ];
}
