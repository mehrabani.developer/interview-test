<?php

namespace Service\Review\Http\Responses;

use Illuminate\Http\Response;
use Service\Review\Contracts\ResponseInterface;
use Service\Review\Http\Resources\ReviewResource;

class AndroidResponses implements ResponseInterface
{
    public function index($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'False',
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function successStore($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'False',
            'status' => 'success',
            'data' => new ReviewResource($data)
        ], Response::HTTP_CREATED);
    }

    public function failedStore($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }

    public function successShow($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'False',
            'status' => 'success',
            'data' => new ReviewResource($data)
        ]);
    }

    public function failedShow($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }

    public function successUpdate($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'False',
            'status' => 'success',
            'data' => new ReviewResource($data)
        ], Response::HTTP_CREATED);
    }

    public function failedUpdate($data = null)
    {
        return response()->json([
            'platform' => 'Android',
            'error' => 'True',
            'status' => 'failed',
            'data' => $data
        ], Response::HTTP_BAD_REQUEST);
    }
}
