<?php

namespace Service\Review\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Service\Product\Contracts\ProductRepository;
use Service\Review\Contracts\ReviewRepository;
use Service\Review\Http\Requests\ReviewStoreRequest;
use Service\Review\Http\Requests\ReviewUpdateRequest;
use Service\Review\Http\Responses\Responses;

class ReviewController
{
    private $reviewRepository;
    private $productRepository;

    public function __construct(
        ReviewRepository $reviewRepository,
        ProductRepository $productRepository
    )
    {
        $this->reviewRepository = $reviewRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Service\Review\Http\Responses\Responses
     */
    public function index()
    {
        return Responses::index(
            $this->reviewRepository->paginate(request('size', 20))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ReviewStoreRequest $request
     * @return \Service\Review\Http\Responses\Responses
     */
    public function store(ReviewStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $review = $this->reviewRepository->create($request->all());
            DB::commit();

            $response = Responses::successStore($review);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedStore();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Service\Review\Http\Responses\Responses
     */
    public function show($id)
    {
        try {
            $response = Responses::successShow(
                $this->reviewRepository->find($id)
            );
        } catch (\Exception $e) {
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedShow($id);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ReviewUpdateRequest $request
     * @param int $id
     * @return \Service\Review\Http\Responses\Responses
     */
    public function update(ReviewUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $review = $this->reviewRepository->update($id, $request->all());
            $this->productRepository->setTotalCommentAndAvgVote($review->product_id);
            DB::commit();

            $response = Responses::successUpdate($review);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedUpdate($id);
        }

        return $response;
    }
}
