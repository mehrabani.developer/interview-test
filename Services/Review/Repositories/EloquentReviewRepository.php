<?php

namespace Service\Review\Repositories;

use Illuminate\Support\Facades\DB;
use Service\Review\Contracts\ReviewRepository;
use Service\Review\Models\Review;

class EloquentReviewRepository implements ReviewRepository
{
    public function paginate($perPage)
    {
        return Review::paginate($perPage);
    }

    public function create($data)
    {
        return Review::create($data);
    }

    public function update($id, $data)
    {
        $review = Review::find($id);
        $review->update($data);
        return $review;
    }

    public function find($id)
    {
        return Review::find($id);
    }

    public function getAvgAndCountWhereProductId($productId)
    {
        return DB::table('reviews')->selectRaw(DB::raw("COUNT(*) AS total_comment,AVG(vote) AS avg_vote"))
            ->where('product_id', $productId)
            ->where('status', 1)
            ->get();
    }
}
