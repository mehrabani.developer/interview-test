<?php

use Illuminate\Support\Facades\Route;
use Service\Review\Http\Controllers\ReviewController;

Route::group(['prefix' => 'api'], function () {

    Route::get('reviews', [ReviewController::class, 'index']);
    Route::post('reviews', [ReviewController::class, 'store']);
    Route::get('reviews/{review}', [ReviewController::class, 'show']);

    // change status (approve OR disapprove)
    Route::patch('reviews/{review}', [ReviewController::class, 'update']);
});
