<?php

namespace Service\Review;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Service\Product\Models\Product;
use Service\Review\Contracts\ReviewRepository;
use Service\Review\Models\Review;
use Service\Review\Repositories\EloquentReviewRepository;

class ReviewServiceProvider extends ServiceProvider
{
    public function register()
    {
        Product::resolveRelationUsing('reviews', function ($review) {
            return $review->hasMany(Review::class)
                ->select(['id', 'user_id', 'product_id', 'comment', 'vote', 'status', 'created_at'])
                ->orderBy('id', 'desc')
                ->take(3);
        });
        $this->app->singleton(ReviewRepository::class, EloquentReviewRepository::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom([
            base_path('Services/Review/Database/migrations')
        ]);

        Route::group([], base_path('Services/Review/routes/api.php'));
    }
}
