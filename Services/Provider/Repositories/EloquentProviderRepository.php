<?php

namespace Service\Provider\Repositories;

use Service\Provider\Contracts\ProviderRepository;
use Service\Provider\Models\Provider;

class EloquentProviderRepository implements ProviderRepository
{
    public function paginate($perPage)
    {
        return Provider::paginate($perPage);
    }

    public function create($data)
    {
        return Provider::create($data);
    }

    public function update($id, $data)
    {
        $review = Provider::find($id);
        $review->update($data);
        return $review;
    }

    public function find($id)
    {
        return Provider::find($id);
    }
}
