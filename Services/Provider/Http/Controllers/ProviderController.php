<?php

namespace Service\Provider\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Service\Provider\Contracts\ProviderRepository;
use Service\Provider\Http\Requests\ProviderStoreRequest;
use Service\Provider\Http\Requests\ProviderUpdateRequest;
use Service\Provider\Http\Responses\Responses;

class ProviderController
{
    private $providerRepository;

    public function __construct(
        ProviderRepository $providerRepository
    )
    {
        $this->providerRepository = $providerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Service\Provider\Http\Responses\Responses
     */
    public function index()
    {
        return Responses::index(
            $this->providerRepository->paginate(request('size', 20))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProviderStoreRequest $request
     * @return \Service\Provider\Http\Responses\Responses
     */
    public function store(ProviderStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $provider = $this->providerRepository->create($request->all());
            DB::commit();
            $response = Responses::successStore($provider);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedStore();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Service\Provider\Http\Responses\Responses
     */
    public function show($id)
    {
        try {
            $response = Responses::successShow(
                $this->providerRepository->find($id)
            );
        } catch (\Exception $e) {
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedShow($id);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProviderUpdateRequest $request
     * @param int $id
     * @return \Service\Provider\Http\Responses\Responses
     */
    public function update(ProviderUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $provider = $this->providerRepository->update($id, $request->all());
            DB::commit();
            $response = Responses::successUpdate($provider);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedUpdate($id);
        }

        return $response;
    }
}
