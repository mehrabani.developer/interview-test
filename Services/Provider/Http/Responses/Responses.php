<?php

namespace Service\Provider\Http\Responses;

use Illuminate\Support\Facades\Facade;

class Responses extends Facade
{
    protected static function getFacadeAccessor() : string
    {
        $clientName = request()->ajax() ? 'ajax' : request('client_name');

        return [
                'android' => AndroidResponses::class,
                'react' => ReactResponses::class,
            ][$clientName] ?? DefaultResponses::class;
    }
}
