<?php

use Illuminate\Support\Facades\Route;
use Service\Provider\Http\Controllers\ProviderController;

Route::group(['prefix' => 'api'], function () {
    Route::get('providers', [ProviderController::class, 'index']);
    Route::post('providers', [ProviderController::class, 'store']);
    Route::get('providers/{provider}', [ProviderController::class, 'show']);
    Route::patch('providers/{provider}', [ProviderController::class, 'update']);
});
