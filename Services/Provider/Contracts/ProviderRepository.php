<?php

namespace Service\Provider\Contracts;

interface ProviderRepository
{
    public function paginate($perPage);
    public function create($data);
    public function update($id, $data);
    public function find($id);
}
