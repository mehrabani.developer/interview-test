<?php

namespace Service\Provider;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Service\Provider\Contracts\ProviderRepository;
use Service\Provider\Repositories\EloquentProviderRepository;

class ProviderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ProviderRepository::class, EloquentProviderRepository::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom([
            base_path('Services/Provider/Database/migrations')
        ]);

        Route::group([], base_path('Services/Provider/routes/api.php'));
    }
}
