<?php

namespace Service\Provider\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $fillable = [
      'name'
    ];
}
