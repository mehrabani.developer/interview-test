<?php

namespace Service\Product\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
      'name',
      'status',
      'provider_id',
      'ability_comment',
      'ability_vote',
      'can_review',
      'avg_votes',
      'total_comments'
    ];
}
