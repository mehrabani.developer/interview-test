<?php

namespace Service\Product\Repositories;

use Service\Product\Contracts\ProductRepository;
use Service\Product\Jobs\CalculateAvgAndCountReviewJob;
use Service\Product\Models\Product;

class EloquentProductRepository implements ProductRepository
{
    public function paginate($perPage)
    {
        return Product::paginate($perPage);
    }

    public function create($data)
    {
        return Product::create($data);
    }

    public function update($id, $data)
    {
        $review = Product::find($id);
        $review->update($data);
        return $review;
    }

    public function find($id)
    {
        return Product::with('reviews')->find($id);
    }

    public function setTotalCommentAndAvgVote($productId)
    {
        return CalculateAvgAndCountReviewJob::dispatch($productId);
    }
}
