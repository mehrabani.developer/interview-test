<?php

namespace Service\Product;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Service\Product\Contracts\ProductRepository;
use Service\Product\Repositories\EloquentProductRepository;

class ProductServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(ProductRepository::class, EloquentProductRepository::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom([
            base_path('Services/Product/Database/migrations')
        ]);

        Route::group([], base_path('Services/Product/routes/api.php'));
    }
}
