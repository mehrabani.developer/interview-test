<?php

namespace Service\Product\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Service\Product\Contracts\ProductRepository;
use Service\Product\Http\Requests\ProductStoreRequest;
use Service\Product\Http\Requests\ProductUpdateRequest;
use Service\Product\Http\Responses\Responses;

class ProductController
{
    private $productRepository;

    public function __construct(
        ProductRepository $productRepository
    )
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Service\Product\Http\Responses\Responses
     */
    public function index()
    {
        return Responses::index(
            $this->productRepository->paginate(request('size', 20))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductStoreRequest $request
     * @return \Service\Product\Http\Responses\Responses
     */
    public function store(ProductStoreRequest $request)
    {
        try {
            DB::beginTransaction();
            $product = $this->productRepository->create($request->all());
            DB::commit();
            $response = Responses::successStore($product);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedStore();
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Service\Product\Http\Responses\Responses
     */
    public function show($id)
    {
        try {
            $response = Responses::successShow(
                $this->productRepository->find($id)
            );
        } catch (\Exception $e) {
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedShow($id);
        }

        return $response;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param int $id
     * @return \Service\Product\Http\Responses\Responses
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $product = $this->productRepository->update($id, $request->all());
            DB::commit();
            $response = Responses::successUpdate($product);
        } catch (\Exception $e) {
            // rollback
            DB::rollBack();
            // log exception
            Log::error($e->getMessage());
            $response = Responses::failedUpdate($id);
        }

        return $response;
    }
}
