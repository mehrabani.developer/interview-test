<?php

namespace Service\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'provider_id' => ['required', 'numeric'],
            'ability_comment' => ['required'],
            'ability_vote' => ['required'],
            'can_review' => ['required', 'in:all_users,purchaser_only']
        ];
    }
}
