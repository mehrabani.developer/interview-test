<?php

namespace Service\Product\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $review = [];
        if(! in_array($request->route()->getName(), ['api.products.store', 'api.products.update'])) {
            $review = ['reviews' => $this->reviews];
        }

        return [
            'name' => $this->name,
            'status' => $this->status,
            'provider_id' => $this->provider_id,
            'ability_comment' => $this->ability_comment,
            'ability_vote' => $this->ability_vote,
            'can_review' => $this->can_review,
            'avg_votes' => $this->avg_votes,
            'total_comments' => $this->total_comments,
        ] + $review;
    }
}
