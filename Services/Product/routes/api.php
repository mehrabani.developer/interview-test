<?php

use Illuminate\Support\Facades\Route;
use Service\Product\Http\Controllers\ProductController;

Route::group(['prefix' => 'api', 'as' => 'api.'], function () {

    Route::get('products', [ProductController::class, 'index']);
    Route::post('products', [ProductController::class, 'store'])
    ->name('products.store');
    Route::get('products/{product}', [ProductController::class, 'show']);

    // change status (show OR hide)
    Route::patch('products/{product}', [ProductController::class, 'update'])
        ->name('products.update');
});
