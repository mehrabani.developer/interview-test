<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('status')->default(false)->index();
            $table->unsignedBigInteger('provider_id')->index();
            $table->boolean('ability_comment')->index();
            $table->boolean('ability_vote')->index();
            $table->enum('can_review', ['purchaser_only', 'all_users'])->index();
            $table->decimal('avg_votes', 5,2)->default(0);
            $table->unsignedInteger('total_comments')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
