<?php

namespace Service\Product\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Service\Product\Contracts\ProductRepository;
use Service\Review\Contracts\ReviewRepository;

class CalculateAvgAndCountReviewJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $productId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $countAndAvgVotes = resolve(ReviewRepository::class)->getAvgAndCountWhereProductId($this->productId)[0];
        resolve(ProductRepository::class)->update($this->productId, [
            'avg_votes' => $countAndAvgVotes->avg_vote,
            'total_comments' => $countAndAvgVotes->total_comment,
        ]);
    }
}
