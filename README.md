# interview test

## Installation

> Note:
> You need PHP v7.4.0+, MySQL v8.0+, composer installed.
1. Clone the repo `git clone https://gitlab.com/mehrabani.developer/interview-test.git`
2. Create a database in MySQL.
3. Copy the `.env.exapmle` to `.env`.

```dotenv
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

5. Install composer dependencies.
6. Generate app key.
7. Migrate the database.
8. Run `php artisan serve` to see the app on [http://127.0.0.1:8000](http://127.0.0.1:8000)



## Description

- Back end code is inside `Services` directory.

## Api Document Link

[postman document Link](https://documenter.getpostman.com/view/16995623/UV5WEJ5G)
